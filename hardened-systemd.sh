#!/bin/bash
# The repository name.
repository_name=${1}
# The URI to which the repository was synced.
sync_uri=${2}
# The path to the repository.
repository_path=${3}

# Portage assumes that a hook succeeded if it exits with 0 code. If no
# explicit exit is done, the exit code is the exit code of last spawned
# command. Since our script is a bit more complex, we want to control
# the exit code explicitly.
ret=0


mystr="# Experimental Hardened/systemd profiles\n"
mystr="${mystr}# All \"supported\" architectures\n"
mystr="${mystr}# @MAINTAINER: gnudicebox@gmail.com\n"
profilespath="/var/db/repos/gentoo/profiles/"
#journal=

if [ "$(grep "hardened/systemd" /var/db/repos/gentoo/profiles/profiles.desc)" != "" ]; then
    # We probably do not need to run this script.
    # Return explicit status.
    exit "${ret}"
fi

for d in $(grep hardened /var/db/repos/gentoo/profiles/profiles.desc  | awk '{print $2}'); do
	if [ "$(grep "features/hardened" "${profilespath}""${d}"/parent)" != "" ]; then
		#printf "${d}\n"
		#echo
		#read
		mkdir -v "${profilespath}""${d}"/systemd
		#printf "${profilespath}${d}/parent:\n"
		#cat "${profilespath}""${d}"/parent
		#echo
		#read
		echo 5 > "${profilespath}""${d}"/systemd/eapi
		#printf "${profilespath}${d}/systemd/eapi:\n"
		#cat "${profilespath}""${d}"/systemd/eapi
		#echo
		#read
		systemdpath="$(realpath --relative-to="${profilespath}""${d}"/systemd /var/db/repos/gentoo/profiles/targets/systemd)"
		sed -e "s/^\.\.$/&\/\.\./" -e "s/^.*features\/hardened.*$/\.\.\/&/" -e "s;^.*features\/hardened.*$;${systemdpath}\n&;" "${profilespath}""${d}"/parent > "${profilespath}""${d}"/systemd/parent
		#printf "${profilespath}${d}/systemd/parent:\n"
		#cat "${profilespath}""${d}"/systemd/parent
    chown -Rv portage:portage "${profilespath}""${d}"/systemd
		#echo
		#read
		if [ "$(echo "${d}" | grep "default/linux/amd64/")" != "" ]; then
			mystr="${mystr}amd64\t\t""${d}""/systemd\t\t\texp\n"
		fi
		if [ "$(echo "${d}" | grep "default/linux/x86/")" != "" ]; then
			mystr="${mystr}x86\t\t""${d}""/systemd\t\t\texp\n"
		fi
    if [ "$(echo "${d}" | grep "default/linux/arm/")" != "" ]; then
			mystr="${mystr}arm\t\t""${d}""/systemd\t\t\texp\n"
		fi
    if [ "$(echo "${d}" | grep "default/linux/arm64/")" != "" ]; then
			mystr="${mystr}arm64\t\t""${d}""/systemd\t\t\texp\n"
		fi
		#printf "${mystr}"
		#echo
		#read
	fi
done


#printf "We are about to modify profiles.desc\n"
#printf "${mystr}"
#echo
#read
sed -i "s;# ARM Profiles;${mystr}\n&;" /var/db/repos/gentoo/profiles/profiles.desc

# Return explicit status.
exit "${ret}"
